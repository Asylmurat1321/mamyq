import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Articls from '../views/Articls.vue';
import ShopPets from '../views/ShopPets.vue';
import ShopProducts from '../views/ShopProducts.vue';
import Buildings from '../views/Buildings.vue';
import ShopPets2 from '../views/shopPets2.vue';
import Buildings2 from '../views/buildings2.vue';
import ShopProducts2 from '../views/shopProducts2.vue';
import Pending from '../views/Pending.vue';
import Shopped from '../views/Shopped.vue';
import Delivered from '../views/Delivered.vue';
import Transmitted from '../views/Transmitted.vue';


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/articls',
    name: 'Articls',
    component: Articls, 
    meta: { requiresAuth: true }
  },
  {
    path: '/shopPets',
    name: 'ShopPets',
    component: ShopPets, 
    meta: { requiresAuth: true }
  },
  {
    path: '/shopProducts',
    name: 'ShopProducts',
    component: ShopProducts, 
    meta: { requiresAuth: true }
  },
  {
    path: '/buildings',
    name: 'Buildings',
    component: Buildings, 
    meta: { requiresAuth: true }
  },
  {
    path: '/shopPets2',
    name: 'ShopPets2',
    component: ShopPets2, 
    meta: { requiresAuth: true }
  },
  {
    path: '/shopProducts2',
    name: 'ShopProducts2',
    component: ShopProducts2, 
    meta: { requiresAuth: true }
  },
  {
    path: '/buildings2',
    name: 'Buildings2',
    component: Buildings2, 
    meta: { requiresAuth: true }
  },
  {
    path: '/pending',
    name: 'Pending',
    component: Pending, 
    meta: { requiresAuth: true }
  },{
    path: '/shopped',
    name: 'Shopped',
    component: Shopped, 
    meta: { requiresAuth: true }
  },{
    path: '/delivered',
    name: 'Delivered',
    component: Delivered, 
    meta: { requiresAuth: true }
  },{
    path: '/transmitted',
    name: 'Transmitted',
    component: Transmitted, 
    meta: { requiresAuth: true }
  },

  
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const isAuthenticated = localStorage.getItem('authToken');

  if (requiresAuth && !isAuthenticated) {
    next('/login');
  } else {
    next();
  }
});

export default router;
